package com.example.demo.Model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id ;

    @Column(name = "car_code", unique = true)
    private String carCode ;

    @Column(name = "car_name")
    private String carName ;

    // phải mapped cái Car car tạo bên class CarType mới đúng 
    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
 /*
  * @JsonManagedReference được xem như là thành phần chính trong mối quan hệ trong serialization, vì vậy chúng sẽ được serialized bình thường. 
    @JsonBackReference được xem như phần phụ trợ, và nó sẽ bị lược bỏ trong serialization để tránh tình trạng lặp vô tận.

  */

    @JsonManagedReference
	private Set<CarType> carType;

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCarCode() {
        return carCode;
    }

    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Set<CarType> getCarType() {
        return carType;
    }

    public void setCarType(Set<CarType> carType) {
        this.carType = carType;
    }

    public Car() {
    }

    public Car(long id, String carCode, String carName, Set<CarType> carType) {
        this.id = id;
        this.carCode = carCode;
        this.carName = carName;
        this.carType = carType;
    }

    

}
