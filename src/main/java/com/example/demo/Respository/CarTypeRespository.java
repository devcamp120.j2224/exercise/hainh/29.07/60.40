package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CarType;

public interface CarTypeRespository extends JpaRepository<CarType , Long> {
    
}
